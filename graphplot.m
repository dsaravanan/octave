#!/usr/bin/env octave
% File: graphplot.m
% Name: D.Saravanan
% Date: 19/08/2021
% Plot the graph of a unit circle

graphics_toolkit("gnuplot")

t = linspace(0, 2*pi, 50)
x = cos(t)
y = sin(t)

plot(x, y, "linewidth", 5)

grid on

print('graphplot.png', '-dpng')
