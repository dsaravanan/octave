#!/usr/bin/env octave
% File: sincfun.m
% Name: D.Saravanan
% Date: 19/08/2021
% Script to plot normalized sinc function

graphics_toolkit("gnuplot")

%function yval = sinc(value)
%    yval = sin(pi*value)/(pi*value)
%end

x = -2*pi:0.001:2*pi
y = sinc(x)

plot(x, y, "linewidth", 2, "color", "red")
legend("sinc(x)")
set(gca, "xlim", [-7,7], "ylim", [-0.3,1.1])
set(gca, "xlabel", text("string", "x", "fontsize", 11))
set(gca, "ylabel", text("string", "sinc(x)", "fontsize", 11))
set(gca, "title", text("string", "sinc function", "fontsize", 12))
grid on
print('sincplot.png', '-dpng')
