#!/usr/bin/env octave
% File: plotting.m
% Name: D.Saravanan
% Date: 04/08/2021
% Two-dimensional plotting

graphics_toolkit("gnuplot")

x = [-5.5:0.1:1]
f = sin(x)

plot(x, f, "linewidth", 5)

% set x-axis limit
set(gca, "xlim", [-5 1])

% set line width of the window box
set(gca, "linewidth", 2)

% set font size of the numbers on the axis
set(gca, "fontsize", 10)

% axes label
set(gca, "xlabel", text("string", "x", "fontsize", 15))
set(gca, "ylabel", text("string", "sin(x)", "fontsize", 15))

% set legends and title
legend("sin(x)")
set(gca, "title", text("string", "sine plotting", "fontsize", 15))

% set ticks
set(gca, "xtick", [-5:1:1])

grid on

print('graph.png', '-dpng')
